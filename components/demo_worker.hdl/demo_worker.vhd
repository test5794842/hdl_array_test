-- THIS FILE WAS ORIGINALLY GENERATED ON Thu Apr 25 10:59:54 2024 BST
-- BASED ON THE FILE: demo_worker.xml
-- YOU *ARE* EXPECTED TO EDIT IT
-- This file initially contains the architecture skeleton for worker: demo_worker

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of worker is
begin
  ctl_out.finished <= btrue; -- remove or change this line for worker to be finished when appropriate
                             -- workers that are never "finished" need not drive this signal
  -- Skeleton assignments for demo_worker's volatile properties
  props_out.char_prop <= to_char(1);
  props_out.array_prop_1 <= (to_short(2),to_short(3));
  props_out.array_prop_2 <= (to_short(4),to_short(5));
end rtl;
