/*
 * THIS FILE WAS ORIGINALLY GENERATED ON Thu Apr 25 09:27:37 2024 BST
 * BASED ON THE FILE: demo_proxy-rcc.xml
 * YOU *ARE* EXPECTED TO EDIT IT
 *
 * This file contains the implementation skeleton for the demo_proxy worker in C++
 */

#include "demo_proxy-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Demo_proxyWorkerTypes;

class Demo_proxyWorker : public Demo_proxyWorkerBase {
  RCCResult run(bool /*timedout*/) {
    std::string out;
    slaves.demo_worker.getProperty_char_prop(out);
    log(1, std::string("padding_front: " + out).c_str());

    slaves.demo_worker.getProperty_array_prop_1(out);
    log(1, std::string("array_prop_1: " + out).c_str());

    slaves.demo_worker.getProperty_array_prop_2(out);
    log(1, std::string("array_prop_2: " + out).c_str());

    return RCC_DONE;
  }
};

DEMO_PROXY_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
DEMO_PROXY_END_INFO
